const BASE_URL = "https://devs.digitels.me";

type LoginRequest = {
  username: string;
  password: string;
  type?: string;
};

type SubmitQRRequest = {
  hotel_id: number;
  device_source: string;
  device_destination: string;
  device_command_type: string;
  user_id?: number;
  room_id?: number;
  current_floor: number;
  destination_floor: number;
};

async function login(data: LoginRequest) {
  return fetch(BASE_URL + "/api/admin/user/login", {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify({ ...data, type: data.type ?? "account" }),
  });
}

async function submitQR(data: SubmitQRRequest) {
  const token = localStorage.getItem("token");

  try {
    const response = await fetch(BASE_URL + "/go/v1/admin/device/command", {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        Authorization: "Bearer " + token,
      },
      body: JSON.stringify(data),
    });

    return Promise.resolve(await response.json());
  } catch (error) {
    return Promise.reject(error);
  }
}

async function getCurrentHotelData() {
  try {
    const token = localStorage.getItem("token");
    const hotelId = localStorage.getItem("hotel_id");
    const response = await fetch(
      BASE_URL +
        "/go/v1/admin/rooms?page=1&limit=1000&search=hotel_id:" +
        hotelId,
      {
        method: "GET",
        headers: {
          Authorization: "Bearer " + token,
        },
      }
    );

    return response.json();
  } catch (error) {
    return Promise.reject(error);
  }
}

async function getCurrentDoor(areaString) {
  const token = localStorage.getItem("token");
  const area = areaString ? areaString : "";

  try {
    const response = await fetch(
      `${BASE_URL}/go/v1/admin/door?limit=1000&type=1`,
      {
        method: "GET",
        headers: {
          Authorization: "Bearer " + token,
        },
      }
    );

    return Promise.resolve(await response.json());
  } catch (err) {
    return Promise.resolve(err);
  }
}

async function getRooms() {
  const token = localStorage.getItem("token");
  try {
    const response = await fetch(`${BASE_URL}/go/v1/admin/rooms?limit=1000`, {
      method: "GET",
      headers: {
        Authorization: "Bearer " + token,
      },
    });

    return Promise.resolve(await response.json());
  } catch (err) {
    return Promise.resolve(err);
  }
}

async function getDoors() {
  const token = localStorage.getItem("token");
  try {
    const response = await fetch(
      `${BASE_URL}/go/v1/admin/door?type=1&limit=1000`,
      {
        method: "GET",
        headers: {
          Authorization: "Bearer " + token,
        },
      }
    );

    return Promise.resolve(await response.json());
  } catch (err) {
    return Promise.resolve(err);
  }
}

export {
  login,
  submitQR,
  getCurrentHotelData,
  getCurrentDoor,
  getRooms,
  getDoors,
};
export type { SubmitQRRequest };
