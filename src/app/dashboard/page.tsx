"use client";

import Clock from "@/components/ui/clock";
import ScanQrButton from "@/components/scan-qr-button";
import Image from "next/image";
import { useEffect } from "react";
import { useRouter } from "next/navigation";
import CurrentFloor from "@/components/dashboard/current-floor";
import { useCurrentDoor } from "@/services/queries/door-current";
import CurrentElevator from "@/components/dashboard/current-elevator";
import { useSnapshot } from "valtio";
import { authState } from "@/store/auth";

const DashboardPage = () => {
  const router = useRouter();
  const snap = useSnapshot(authState);
  useCurrentDoor();

  useEffect(() => {
    if (!snap.token) {
      router.replace("/");
    }
  }, [snap])

  return (
    <>
      <header className="w-full px-12 pt-4 max-w-screen-lg mx-auto">
        <div className="flex justify-between items-center pb-2 border-b border-gray-300">
          <Image
            alt="Hotel signature logo"
            src="/signature-logo.png"
            width={180}
            height={42}
          />
          <div className="flex flex-col">
            <Clock />
          </div>
        </div>
      </header>
      <main className="px-12 pt-4 max-w-screen-lg mx-auto">
        <CurrentElevator />
        <div className="flex justify-center gap-2 md:gap-10 mt-8 md:px-16">
          <CurrentFloor />
          <ScanQrButton />
        </div>
        {/* <div className="mt-8">
          <PublicArea />
        </div> */}
      </main>
    </>
  );
};

export default DashboardPage;

{
  /* <div className="mt-4 flex flex-col justify-center items-center">
          <h2 className="text-lg font-medium my-6">
            Please select floor you want to go to public area :
          </h2>

          <PublicBuildingArea />
        </div> */
}
