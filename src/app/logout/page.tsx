"use client"

import { authState } from "@/store/auth";
import { floorState } from "@/store/floor";
import { useRouter } from "next/navigation";
import { useEffect } from "react";

const Logout = () => {

  const router = useRouter();

  useEffect(() => {
    floorState.area = '';
    floorState.floor = '';
    floorState.hotel_id = '';

    authState.token = '';

    router.replace('/');
  }, []);

  return <></>
}

export default Logout;