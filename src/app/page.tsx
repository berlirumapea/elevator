"use client"

import Image from 'next/image'
import { useRouter } from 'next/navigation';
import { useEffect } from 'react';
import LoginForm from '@/components/login-form';
import { useSnapshot } from 'valtio';
import { authState } from '@/store/auth';

export default function Home() {

  const router = useRouter();
  const snap = useSnapshot(authState);

  useEffect(() => {
    if (snap.token) {
      router.replace("/dashboard");
    }
  }, [])

  return (
    <main className='home-with-bg'>
      <div className='z-10 relative w-full flex items-center justify-center pt-12'>
        <Image alt="digitels logo" src="/digitels-logo.png" width={150} height={134} />
      </div>

      <div className='absolute bottom-0 left-0 right-0 w-full h-[65%] bg-white rounded-tl-[100px] z-10 flex items-center justify-center flex-col'>
        <LoginForm />

      </div>
    </main>
  )
}
