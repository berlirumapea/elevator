"use client";

import { useCallback, useMemo, useRef, useState } from "react";
import { useMutation } from "@tanstack/react-query";
import { useRouter } from "next/navigation";
import QrScanPageHeader from "@/components/qr-page/qr-scan-page-header";
import QrScannerCamera from "@/components/qr-page/qr-scanner";
import { useHotelDoors } from "@/services/queries/hotel-doors";
import { useCurrentDoor } from "@/services/queries/door-current";
import { SubmitQRRequest, submitQR } from "@/services/queries/qr";
import { useHotelRooms } from "@/services/queries/hotel-rooms";
import { Progress } from "@/components/ui/progress";
import { toast } from "sonner";
import { useHotelElevator } from "@/services/queries/hotel-elevators";
import { useSnapshot } from "valtio";
import { floorState } from "@/store/floor";

const ScanPage = () => {
  const scannedResult = useRef("");
  const [availableFloors, setAvailableFloors] = useState<number[]>([]);
  const [currentQrResult, setCurrentQrResult] = useState<string[]>([]);
  const router = useRouter();
  const snap = useSnapshot(floorState);

  const { data: doors, isFetched: doorsFetched } = useHotelDoors();
  const { data: rooms, isFetched: roomsFetched } = useHotelRooms();
  const { data: currentDoor, isFetched: currentDoorFetched } = useCurrentDoor();
  const { data: elevators, isFetched: elevatorFetched } = useHotelElevator();

  const { mutate: submit } = useMutation({
    mutationFn: async (data: SubmitQRRequest) => await submitQR(data),
    onSuccess: (response) => {
      if (response.code === 400) {
        toast.error(response.message || "Something went wrong");
      } else if (response.code === 409) {
        setAvailableFloors(response?.data || []);
      } else if (response.code === 200) {
        goToDestination();
        router.push("/dashboard");
      }

      scannedResult.current = "";
    },
    onError: (error) => {
      console.log("Error at: ", error);
      scannedResult.current = "";
    },
  });

  const goToDestination = () => {
    const selectedUserId = Number(currentQrResult[1]);
    const selectedRoomId = Number(currentQrResult[0]);

    // find destination room
    const destinationRoom = rooms?.data?.find(
      (room: any) => room.id === selectedRoomId
    );

    // find door location when room is found
    const destinationDoor = doors?.data?.find(
      (door: any) => door.floor === destinationRoom?.floor
    );

    // show error if
    if (!destinationDoor) {
      toast.error("Can not find elevator in destination floor");
      scannedResult.current = "";
      return;
    }

    const body = {
      current_floor: currentDoor?.data?.[0]?.floor,
      device_source: currentDoor?.data?.[0]?.mac_control,
      hotel_id: Number(snap.hotel_id),
      device_command_type: "unlockLift", // TODO: where to get this value??
      device_destination: currentDoor?.data?.[0]?.mac_control,

      destination_floor: destinationDoor?.floor,
      room_id: selectedRoomId,
      user_id: selectedUserId ? selectedUserId : undefined,
    };

    submit(body);
  };

  const scanOnDecode = (result: string) => {
    console.log("result", result);

    if (scannedResult.current !== "" && scannedResult.current === result)
      return false;
    scannedResult.current = result;

    // extract data from QR code
    let finalResult = result.split(":")[1].split(";");

    setCurrentQrResult(finalResult);

    if (finalResult.length === 2) {
      // if it 2, then it contains room id

      const selectedUserId = Number(finalResult[1]);
      const selectedRoomId = Number(finalResult[0]);

      // find destination room
      const destinationRoom = rooms?.data?.find(
        (room: any) => room.id === selectedRoomId
      );

      // find door location when room is found
      // const destinationDoor = elevators?.data?.find((door: any) => door.floor === destinationRoom?.floor);

      submit({
        hotel_id: Number(snap.hotel_id),
        current_floor: Number(snap.floor),
        destination_floor: Number(destinationRoom?.floor),
        device_command_type: "unlockLift", // TODO: where to get this value??
        device_source: currentDoor?.data?.[0]?.mac_control,
        device_destination: currentDoor?.data?.[0]?.mac_control,
        user_id: selectedUserId,
        room_id: selectedRoomId,
      });
    } else {
      submit({
        hotel_id: Number(snap.hotel_id),
        current_floor: Number(snap.floor),
        device_command_type: "unlockLift", // TODO: where to get this value??
        device_source: currentDoor?.data?.[0]?.mac_control,
        device_destination: currentDoor?.data?.[0]?.mac_control,
        user_id: Number(finalResult[0]), // if it only contains 1 item, then it's a user_id
      });
    }
  };

  const onPageClose = () => {
    router.push("/dashboard");
  };

  const onFloorClick = (floor: number) => {
    const selectedUserId = Number(currentQrResult[1]);
    const selectedRoomId = Number(currentQrResult[0]);
    submit({
      hotel_id: Number(snap.hotel_id),
      current_floor: Number(snap.floor),
      device_command_type: "unlockLift", // TODO: where to get this value??
      device_source: currentDoor?.data?.[0]?.mac_control,
      device_destination: currentDoor?.data?.[0]?.mac_control,
      destination_floor: floor,
      user_id: selectedUserId,
      room_id: selectedRoomId,
    });
  };

  const handleInitScanner = (test) => {
    console.log("initScanner", test);
  };

  const progress = useMemo(() => {
    const vars = [doorsFetched, roomsFetched, currentDoorFetched];
    const trueCount = vars.filter((value) => value).length;

    return (trueCount / vars.length) * 100;
  }, [doorsFetched, roomsFetched, currentDoorFetched]);

  return (
    <div className="fixed top-0 left-0 right-0 bottom-0">
      <QrScanPageHeader onClose={onPageClose} />
      <div className="pt-20 h-screen absolute top-0 left-0 right-0 bottom-0 flex items-center w-full z-10">
        <div className="w-full bg-white h-full max-w-screen-md mx-auto flex items-start pt-8 px-8 justify-center">
          {progress === 100 ? (
            <QrScannerCamera
              onInit={handleInitScanner}
              onResult={(data) => scanOnDecode(data.data)}
            />
          ) : (
            <div className="flex flex-col justify-center items-center mt-16 w-full">
              <p className="text-sm font-semibold">Loading data...</p>
              <Progress className="mt-4" value={progress} />
            </div>
          )}
        </div>
      </div>

      {availableFloors.length > 0 && (
        <div className="w-full h-full fixed left-0 top-0 bottom-0 bg-black/70 z-50 flex flex-col justify-end">
          <div className="h-[300px] bg-white rounded-tl-3xl rounded-tr-3xl px-8 flex flex-col items-center justify-items-center py-8">
            <h1 className="text-xl font-bold mb-1">Please choose your floor</h1>
            <p className="text-sm mb-10 text-center">
              You have more than one room in this property, please select one of
              to guide you to your room floor
            </p>
            <div className="flex flex-wrap gap-2 w-full px-8">
              {Array.from(new Set(availableFloors)).map((floor, idx) => (
                <div
                  key={idx}
                  onClick={() => onFloorClick(floor)}
                  className="min-w-[180px] bg-gray-800 hover:bg-gray-950 hover:ring hover:ring-offset-1 hover:ring-gray-950 active:bg-gray-800 h-[100px] rounded-xl flex items-center justify-center"
                >
                  <h1 className="text-5xl font-bold text-white">{floor}</h1>
                </div>
              ))}
            </div>
          </div>
        </div>
      )}
    </div>
  );
};

export default ScanPage;
