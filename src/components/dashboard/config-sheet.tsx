"use client"

import { Button } from "@/components/ui/button"
import { Input } from "@/components/ui/input"
import { Label } from "@/components/ui/label"
import {
  Sheet,
  SheetContent,
  SheetDescription,
  SheetFooter,
  SheetHeader,
  SheetTitle,
  SheetTrigger,
} from "@/components/ui/sheet"
import { useForm } from "react-hook-form"
import { z } from "zod"
import { zodResolver } from "@hookform/resolvers/zod"
import { useSnapshot } from "valtio"
import { floorState } from "@/store/floor"
import { useEffect, useState } from "react"
import { cn } from "@/lib/utils"

const schema = z.object({
  floor: z.string().min(1, 'Field is required'),
  area: z.string().min(1, 'Field is required'),
})

type ConfigForm = z.infer<typeof schema>

type ConfigSheetProps = {
  open: boolean;
  onOpenChange: (open: boolean) => void;
}

export function ConfigSheet({ open, onOpenChange }: ConfigSheetProps) {

  const snap = useSnapshot(floorState);
  const [isMounted, setIsMounted] = useState(false);

  const { register, formState: { errors }, handleSubmit } = useForm<ConfigForm>({
    defaultValues: { area: snap.area, floor: snap.floor },
    resolver: zodResolver(schema)
  });

  const onSubmit = handleSubmit((data) => {
    floorState.area = data.area;
    floorState.floor = data.floor;
    onOpenChange(false);
  });

  useEffect(() => {
    setIsMounted(true);
  }, []);

  return (
    <Sheet open={open} onOpenChange={onOpenChange}>
      <SheetTrigger asChild>
        {isMounted &&
          <Button className={cn("mt-4 w-full", snap.floor ? "text-gray-400" : "text-gray-800")} variant="outline" size="sm">
            Set Floor Config {snap.floor && " (Set)"}
          </Button>
        }
      </SheetTrigger>
      <SheetContent>
        <SheetHeader>
          <SheetTitle>Edit device config</SheetTitle>
          <SheetDescription>This is to define tablet/device position in the building.</SheetDescription>
        </SheetHeader>
        <form onSubmit={onSubmit}>
          <div className="grid gap-6 py-4">
            <div className="flex flex-col">
              <Label htmlFor="floor" className="mb-2">
                Floor
              </Label>
              <Input id="floor" {...register("floor")} className="mb-1" />
              {errors.floor && <span className='text-xs text-red-500'>{errors.floor.message}</span>}
            </div>
            <div className="flex flex-col">
              <Label htmlFor="area" className="mb-2">
                Area
              </Label>
              <Input id="area" {...register("area")} className="mb-1" />
              {errors.area && <span className='text-xs text-red-500'>{errors.area.message}</span>}
            </div>
          </div>
          <SheetFooter>
            <Button type="submit">Save changes</Button>
          </SheetFooter>
        </form>
      </SheetContent>
    </Sheet>
  )
}
