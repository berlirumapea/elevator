"use client"

import { floorState } from "@/store/floor";
import React, { useEffect, useState } from "react";
import { useSnapshot } from "valtio";

const CurrentElevator = () => {
  const [mounted, setMounted] = useState(false);
  const snap = useSnapshot(floorState);

  // to surpass hydration warning
  useEffect(() => {
    setMounted(true);
  }, []);

  return (
    <h1 className="text-2xl font-semibold text-center mb-8 mt-4">You are in elevator {mounted && <span className="uppercase">{snap.area}</span>}</h1>
  )
}

export default CurrentElevator;