"use client";

import { floorState } from "@/store/floor";
import React from "react";
import { useSnapshot } from "valtio";

const CurrentFloor = () => {
  const snap = useSnapshot(floorState);

  return (
    <div className="w-full max-w-[300px] bg-white rounded-lg border border-gray-300 h-[250px] flex items-center justify-center flex-col px-4 text-center">
      <h2 className="text-xl mb-8 font-semibold">Welcome! <br />You are on floor</h2>
      <h1 className="text-gray-500 text-5xl font-semibold">{snap.floor}</h1>
    </div>
  );
};

export default CurrentFloor;
