const className = "rounded-full border border-gray-400 flex items-center justify-center flex-col hover:bg-gray-100 active:bg-gray-200 transition h-[140px] w-[140px]";

const PublicArea = () => {
  return (
    <div className="flex flex-wrap gap-4 items-center justify-center">
      <button className={className}>
        <h3 className="text-lg">B1</h3>
        <p className="text-sm text-gray-500">Basement</p>
      </button>
      <button className={className}>
        <h3 className="text-lg">B2</h3>
        <p className="text-sm text-gray-500">Lobby Area</p>
      </button>
      <button className={className}>
        <h3 className="text-lg">B3</h3>
        <p className="text-sm text-gray-500">Swimming Pool</p>
      </button>
    </div>
  );
}

export default PublicArea;