"use client"

import { useForm } from 'react-hook-form';
import { z } from 'zod';
import { zodResolver } from '@hookform/resolvers/zod';
import { useMutation } from '@tanstack/react-query';
import { Input } from './ui/input';
import { Button } from './ui/button';
import { toast } from 'sonner';
import { useRouter } from 'next/navigation';
import { login } from '@/services/queries/login';
import { useSnapshot } from 'valtio';
import { floorState } from '@/store/floor';
import { authState } from '@/store/auth';
import { ConfigSheet } from './dashboard/config-sheet';
import { useState } from 'react';

const schema = z.object({
  username: z.string().min(2, 'Invalid username'),
  password: z.string().min(6, 'Invalid password'),
  type: z.string().optional(),
})

type LoginSchema = z.infer<typeof schema>;

const LoginForm = () => {

  const router = useRouter();
  const snap = useSnapshot(floorState);
  const [sheetOpen, setSheetOpen] = useState(false);

  const { register, formState: { errors }, handleSubmit, reset } = useForm<LoginSchema>({
    resolver: zodResolver(schema),
  });

  const { mutate, isPending } = useMutation({
    mutationFn: (data: LoginSchema) => login({
      username: data.username,
      password: data.password
    }),
    onSuccess: async (data) => {
      const response = await data.json();
      if (data.status !== 200) {
        toast.error(response['description']);
        reset();
      } else {
        const token = response['result']['token'];
        const hotelId = response['result']['channel']?.split("/")[1];
        authState.token = token;
        floorState.hotel_id = hotelId;
        toast.success("Login success!");
        router.push('/dashboard');
      }
    },
    onError: (error) => {
      console.log("error", error);
      toast.error("Failed!")
    }
  })

  const onSubmit = handleSubmit((data) => {
    if (!snap.floor || !snap.area) {
      toast.error("Set floor config first");
      setSheetOpen(true);
    } else {
      mutate(data);
    }
  });

  return (
    <div className='flex flex-col max-w-[320px]'>
      <form onSubmit={onSubmit} className='flex flex-col w-full items-center justify-center'>
        <h1 className='text-5xl font-semibold mb-3'>Sign In</h1>

        <p className='mb-12 text-gray-500 font-normal'>Hello there! Sign in to acces the elevator</p>

        <fieldset className='mb-4 w-full'>
          <Input placeholder='Username' {...register("username")} />
          {errors.username && <span className='text-xs text-red-500'>{errors.username.message}</span>}
        </fieldset>

        <fieldset className='w-full'>
          <Input placeholder='Password' type='password'  {...register("password")} />
          {errors.password && <span className='text-xs text-red-500'>{errors.password.message}</span>}
        </fieldset>

        <Button className='mt-8 w-full' disabled={isPending}>Login</Button>


      </form>
      <ConfigSheet open={sheetOpen} onOpenChange={(open) => setSheetOpen(open)} />
    </div>

  );
}

export default LoginForm;