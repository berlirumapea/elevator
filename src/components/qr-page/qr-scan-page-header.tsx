type QrScanPageHeaderProps = {
  onClose: () => void;
}

const QrScanPageHeader = ({ onClose }: QrScanPageHeaderProps) => {
  return (
    <header className="h-24 bg-black w-full flex items-center z-20 relative px-4">
      <div className="max-w-screen-md flex justify-center items-center w-full mx-auto relative">
        <button onClick={onClose} className="absolute left-0">
          <svg fill="currentColor" className="h-8 w-8 text-white" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg" aria-hidden="true">
            <path d="M6.28 5.22a.75.75 0 00-1.06 1.06L8.94 10l-3.72 3.72a.75.75 0 101.06 1.06L10 11.06l3.72 3.72a.75.75 0 101.06-1.06L11.06 10l3.72-3.72a.75.75 0 00-1.06-1.06L10 8.94 6.28 5.22z" />
          </svg>
        </button>

        <h1 className="text-lg font-semibold text-white">Scan Barcode</h1>

        {/* <button>
          <svg className="h-6 w-6" viewBox="0 0 21 38" fill="none" xmlns="http://www.w3.org/2000/svg">
            <path d="M8.83333 24.0001H0.5L12.1667 0.666748V14.0001H20.5L8.83333 37.3334V24.0001Z" fill="white" />
          </svg>
        </button> */}
      </div>
    </header>
  );
}

export default QrScanPageHeader;