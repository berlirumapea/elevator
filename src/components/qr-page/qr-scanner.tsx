"use client";

import { useEffect, useRef } from "react";
import QrScanner from 'qr-scanner';

type QrScannerCameraProps = {
  onResult: (data: QrScanner.ScanResult) => void,
  onInit?: (scanner: QrScanner) => void;
}

const QrScannerCamera = ({ onResult, onInit }: QrScannerCameraProps) => {
  const scannerRef = useRef<HTMLVideoElement>(null);

  useEffect(() => {
    if (scannerRef.current) {
      const scanner = new QrScanner(scannerRef.current, onResult,
        {
          highlightScanRegion: true,
          highlightCodeOutline: true,
          onDecodeError: (error) => {
            console.log("Error", error);
          },
          maxScansPerSecond: 1,
        }
      );

      scanner.start();

      onInit?.(scanner);
    }
  }, [])

  return (
    <video className="w-full h-[500px]" ref={scannerRef}></video>
  );
}

export default QrScannerCamera;