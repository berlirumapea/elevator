"use client"

import { formatCurrentDate } from "@/lib/utils";
import { useEffect, useRef } from "react";

const Clock = () => {

  const clockRef = useRef<HTMLHeadingElement>(null);
  const currentDate = Date

  useEffect(() => {

    function updateClock() {
      const currentTime = new Date();

      const hours = currentTime.getHours().toString().padStart(2, '0');
      const minutes = currentTime.getMinutes().toString().padStart(2, '0');
      const seconds = currentTime.getSeconds().toString().padStart(2, '0');

      const formattedTime = `${hours}:${minutes}:${seconds}`;

      if (clockRef.current) {
        clockRef.current.innerHTML = formattedTime;
      }
    }

    const id = setInterval(updateClock, 1000);

    return () => {
      clearInterval(id);
    }

  }, [])

  return (
    <div className="text-right">
      <h3 className="text-2xl font-medium mb-0.5" ref={clockRef}>00:00:00</h3>
      <p className="text-xs text-gray-400">{formatCurrentDate()}</p>
    </div>
  )
}

export default Clock;