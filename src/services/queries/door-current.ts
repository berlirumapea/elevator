import { API_BASE_URL } from "@/lib/constant";
import { useQuery } from "@tanstack/react-query";

async function getCurrentDoor() {
  const token = localStorage.getItem("token");
  const area = localStorage.getItem("area");

  try {
    const response = await fetch(
      `${API_BASE_URL}/go/v1/admin/door?limit=1000&type=1&area=${area}`,
      {
        method: "GET",
        headers: {
          Authorization: "Bearer " + token,
        },
      }
    );

    return Promise.resolve(await response.json());
  } catch (err) {
    return Promise.resolve(err);
  }
}

const useCurrentDoor = () => {
  return useQuery({
    queryKey: ["CURRENT_DOOR"],
    queryFn: getCurrentDoor,
  });
};

export { useCurrentDoor };
