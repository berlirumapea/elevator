import { API_BASE_URL } from "@/lib/constant";
import { useQuery } from "@tanstack/react-query";

async function getCurrentHotelData() {
  try {
    const token = localStorage.getItem("token");
    const hotelId = localStorage.getItem("hotel_id");
    const response = await fetch(
      API_BASE_URL +
        "/go/v1/admin/rooms?page=1&limit=1000&search=hotel_id:" +
        hotelId,
      {
        method: "GET",
        headers: {
          Authorization: "Bearer " + token,
        },
      }
    );

    return response.json();
  } catch (error) {
    return Promise.reject(error);
  }
}

const useHotelData = () => {
  return useQuery({
    queryKey: ["HOTEL"],
    queryFn: getCurrentHotelData,
  });
};

export { useHotelData };
