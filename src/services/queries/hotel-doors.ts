import { API_BASE_URL } from "@/lib/constant";
import { useQuery } from "@tanstack/react-query";

async function getDoors() {
  const token = localStorage.getItem("token");

  try {
    const response = await fetch(
      `${API_BASE_URL}/go/v1/admin/door?type=0&limit=1000`,
      {
        method: "GET",
        headers: {
          Authorization: "Bearer " + token,
        },
      }
    );

    return Promise.resolve(await response.json());
  } catch (err) {
    return Promise.resolve(err);
  }
}

const useHotelDoors = () => {
  return useQuery({
    queryKey: ["ALL_DOORS"],
    queryFn: getDoors,
  });
};

export { useHotelDoors };
