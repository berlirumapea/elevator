import { API_BASE_URL } from "@/lib/constant";
import { useQuery } from "@tanstack/react-query";

async function getRooms() {
  const token = localStorage.getItem("token");

  try {
    const response = await fetch(
      `${API_BASE_URL}/go/v1/admin/rooms?limit=1000`,
      {
        method: "GET",
        headers: {
          Authorization: "Bearer " + token,
        },
      }
    );

    return Promise.resolve(await response.json());
  } catch (err) {
    return Promise.resolve(err);
  }
}

const useHotelRooms = () => {
  return useQuery({
    queryKey: ["ALL_ROOMS"],
    queryFn: getRooms,
  });
};

export { useHotelRooms };
