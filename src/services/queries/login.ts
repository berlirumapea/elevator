import { API_BASE_URL } from "@/lib/constant";

type LoginRequest = {
  username: string;
  password: string;
  type?: string;
};

async function login(data: LoginRequest) {
  return fetch(API_BASE_URL + "/api/admin/user/login", {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify({ ...data, type: data.type ?? "account" }),
  });
}

export { login };
