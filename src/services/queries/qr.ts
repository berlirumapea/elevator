import { API_BASE_URL } from "@/lib/constant";

type SubmitQRRequest = {
  hotel_id?: number;
  device_source?: string;
  device_destination?: string;
  device_command_type?: string;
  user_id?: number;
  room_id?: number;
  current_floor?: number;
  destination_floor?: number;
};

async function submitQR(data: SubmitQRRequest) {
  const token = localStorage.getItem("token");

  try {
    const response = await fetch(API_BASE_URL + "/go/v1/admin/device/command", {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        Authorization: "Bearer " + token,
      },
      body: JSON.stringify(data),
    });

    return Promise.resolve(await response.json());
  } catch (error) {
    return Promise.reject(error);
  }
}

export { submitQR };
export type { SubmitQRRequest };
