"use client";

import { proxy, subscribe } from "valtio";

type AuthState = {
  token: string;
};

const authState = proxy<AuthState>({
  token:
    typeof window !== "undefined" ? localStorage.getItem("token") ?? "" : "",
});

subscribe(authState, () => {
  localStorage.setItem("token", authState.token);
});

export { authState };
