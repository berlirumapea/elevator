"use client";

import { proxy, subscribe } from "valtio";

type FloorState = {
  floor: string;
  area: string;
  hotel_id: string;
};

const floorState = proxy<FloorState>({
  floor:
    typeof window !== "undefined" ? localStorage.getItem("floor") ?? "" : "",
  area: typeof window !== "undefined" ? localStorage.getItem("area") ?? "" : "",
  hotel_id:
    typeof window !== "undefined" ? localStorage.getItem("hotel_id") ?? "" : "",
});

subscribe(floorState, () => {
  localStorage.setItem("floor", floorState.floor);
  localStorage.setItem("area", floorState.area);
  localStorage.setItem("hotel_id", floorState.hotel_id);
});

export { floorState };
